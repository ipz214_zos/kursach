import React, { useContext } from 'react';
import { observer } from "mobx-react";
import { Context } from "../index";
import { Card, Row } from "react-bootstrap";
import "./style/styleForComponent.css";
import ListGroup from "react-bootstrap/ListGroup";

const BrandBar = observer(() => {
    const { device } = useContext(Context);

    const handleBrandClick = (brand) => {
        if (device.selectedBrand && brand.id === device.selectedBrand.id) {
            device.setSelectedBrand(1);
        } else {
            device.setSelectedBrand(brand);
        }
    };

    return (
        <ListGroup className={"mt-1"}>
            {device.brands.map((type,index) => (
                <ListGroup.Item
                    active={device.selectedType?.id === type.id}
                    style={{ cursor: 'pointer' ,background: index % 2 === 0 ? '#1A592A' : '#30A64D', padding: 10 }}
                    onClick={() => handleBrandClick(type)}
                    key={type.id}
                >
                    {type.name}
                </ListGroup.Item>
            ))}
        </ListGroup>
    );
});

export default BrandBar;
