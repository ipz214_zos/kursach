import React, { useEffect, useState } from 'react';
import { Button, Card, Col, Container, Image, Row } from "react-bootstrap";

import { useParams } from 'react-router-dom';
import { addToBasket, fetchOneDevice } from "../http/deviceAPI";
import "./style/styleForPages.css";


const DevicePage = () => {
    const [device, setDevice] = useState({ info: [] });
    const [rating, setRating] = useState(0);
    const [basket,setBasket]=useState(0)
    const { id } = useParams();





    useEffect(() => {
        fetchOneDevice(id).then(data => {
            setDevice(data);
            setRating(data.rating);
        });
    }, []);



    const addToBasketHandler = (event) => {
        const Baskets= event.target.value;
        setBasket(Baskets)
        addToBasket ( id );
    }

    return (
        <Container className="devicePageContainer">
            <Row  >
                <Col md={4}>
                    <Image className="ImgItem" width={300} height={350} src={process.env.REACT_APP_API_URL + device.img} />
                </Col>
                <Col md={4}>
                    <Row className="d-flex flex-column align-items-center">
                        <h2 className="text-white">{device.name}</h2>
                    </Row>
                </Col>
                <Col md={4}>
                    <Card
                        className="d-flex flex-column align-items-center justify-content-around"
                        style={{ width: 300, height: 300, fontSize: 32, border: '5px solid lightgray' }}
                    >
                        <h3>Від: {device.price && device.price.toLocaleString()} ₴/день</h3>
                        <Button variant={"outline-dark"} onClick={addToBasketHandler}>Додати в кошик</Button>
                    </Card>
                </Col>
            </Row>
            <Row className="d-flex flex-column m-3">
                <h1 className="text-white">Характеристики</h1>
                {device.info.map((info, index) =>
                    <Row key={info.id} style={{ background: index % 2 === 0 ? '#1A592A' : '#30A64D', padding: 10 }}>
                        {info.title}: {info.description}
                    </Row>
                )}
            </Row>

        </Container>
    );
};

export default DevicePage;
